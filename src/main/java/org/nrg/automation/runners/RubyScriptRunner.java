package org.nrg.automation.runners;

import org.nrg.automation.annotations.Supports;

@Supports("ruby")
public class RubyScriptRunner extends AbstractScriptRunner {
}
