<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.nrg</groupId>
        <artifactId>parent</artifactId>
        <version>1.7.0-SNAPSHOT</version>
    </parent>

    <artifactId>automation</artifactId>

    <name>NRG Automation Framework</name>
    <description>
        Provides an automation framework for running interpreted scripts. This uses the NRG configuration service to
        store scripts. Scripts can be scoped to be associated with any level of entity within a particular system.
    </description>

    <scm>
        <connection>scm:hg:https://bitbucket.org/nrg/nrg_automation</connection>
        <url>scm:hg:https://bitbucket.org/nrg/nrg_automation</url>
    </scm>

    <issueManagement>
        <system>JIRA</system>
        <url>https://issues.xnat.org</url>
    </issueManagement>

    <licenses>
        <license>
            <name>Simplified BSD License</name>
            <url>http://www.opensource.org/licenses/BSD-2-Clause</url>
        </license>
    </licenses>

    <organization>
        <name>Neuroinformatics Research Group</name>
        <url>http://nrg.wustl.edu</url>
    </organization>

    <properties>
        <maven.server.root>https://nrgxnat.artifactoryonline.com/nrgxnat</maven.server.root>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.nrg</groupId>
            <artifactId>framework</artifactId>
        </dependency>
        <dependency>
            <groupId>org.codehaus.groovy</groupId>
            <artifactId>groovy-all</artifactId>
        </dependency>
        <dependency>
            <groupId>org.python</groupId>
            <artifactId>jython-standalone</artifactId>
        </dependency>
        <!--
            TODO: The jruby dependency causes unit tests to fail. First there's a different version of asm that causes errors
            with cglib. Excluding the asm libraries referenced by jruby seems to fix that problem, but then it gets NoClassDefFoundError
            Could not initialize class net.sf.cglib.proxy.Enhancer. I think it's another class missing, not Enhancer, but can't
            tell exactly what. Probably the whole framework just needs an updated cglib and asm dependency.
        -->
        <!-- dependency>
            <groupId>org.jruby</groupId>
            <artifactId>jruby</artifactId>
            <exclusions>
                <exclusion>
                    <groupId>org.ow2.asm</groupId>
                    <artifactId>asm</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.ow2.asm</groupId>
                    <artifactId>asm-commons</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.ow2.asm</groupId>
                    <artifactId>asm-tree</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.ow2.asm</groupId>
                    <artifactId>asm-analysis</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>org.ow2.asm</groupId>
                    <artifactId>asm-util</artifactId>
                </exclusion>
            </exclusions>
        </dependency -->
        <dependency>
            <groupId>org.reflections</groupId>
            <artifactId>reflections</artifactId>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-oxm</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-core</artifactId>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
        </dependency>
        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-annotations</artifactId>
        </dependency>
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-core</artifactId>
        </dependency>
        <dependency>
            <groupId>org.javassist</groupId>
            <artifactId>javassist</artifactId>
        </dependency>
        <dependency>
            <groupId>cglib</groupId>
            <artifactId>cglib</artifactId>
        </dependency>

        <!-- Runtime dependencies -->
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-ehcache</artifactId>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>net.sf.ehcache</groupId>
            <artifactId>ehcache-core</artifactId>
            <scope>runtime</scope>
        </dependency>

        <!-- Provided dependencies -->
        <dependency>
            <groupId>org.jetbrains</groupId>
            <artifactId>annotations</artifactId>
        </dependency>

        <!-- Test dependencies -->
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
        </dependency>
        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
        </dependency>
    </dependencies>

    <build>
        <finalName>nrg-${project.artifactId}-${project.version}</finalName>
        <plugins>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>findbugs-maven-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>maven-source-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>maven-javadoc-plugin</artifactId>
            </plugin>
            <plugin>
                <artifactId>maven-surefire-plugin</artifactId>
            </plugin>
        </plugins>
    </build>

    <repositories>
        <repository>
            <id>org.nrg.xnat.maven.libraries.release</id>
            <name>XNAT Maven Release Libraries</name>
            <url>${maven.server.root}/libs-release</url>
            <releases>
                <enabled>true</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>org.nrg.xnat.maven.libraries.snapshot</id>
            <name>XNAT Maven Snapshot Libraries</name>
            <url>${maven.server.root}/libs-snapshot</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>

</project>
